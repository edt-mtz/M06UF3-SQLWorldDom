package world;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

/**
 *
 * Copyright 2017 ManuMtz
 *
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class WorldReader extends DomReader {

    public WorldReader(String xml) throws ParserConfigurationException, SAXException, IOException {
        super(xml);

    }

    public List<String> listarPaises(){
        return(super.extractList("//country/name/text()"));
    }

    public List<String> listarCiudadesPais(String pais) {
        return(super.extractList("//country[name='"+pais+"']//city/name[1]/text()"));
    }

    public String getPaisId(String pais) {
        return super.extractValue("//country[name ='" + pais + "']/@car_code");

    }

}
package world.postgres;

import world.WorldReader;

import java.sql.*;
import java.util.ArrayList;

import static world.MainWorld.*;

/**
 *
 * Copyright 2017 ManuMtz
 *
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Postgres {

    // PSQL
    public static String dbPostgres = "postgresql";
    public static String dbHost = "localhost";
    public static String db = "world";

    public static void menuPostgres() {
        System.out.println("\n**********************************************");
        System.out.println("*                 PostgreSQL                 *");
        System.out.println("**********************************************");
        System.out.println(String.format("[1] Create/Reset tables: %s and %s", COUNTRY_TABLE, CITY_TABLE));
        System.out.println(String.format("[2] Insert %s & %s from XML", COUNTRY_TABLE, CITY_TABLE));
        System.out.println(String.format("[3] Insert a new %s", COUNTRY_TABLE_FIELD));
        System.out.println(String.format("[4] Insert a new %s", CITY_TABLE_FIELD));
        System.out.println("**********************************************");
        System.out.println(String.format("[5] Remove a %s", COUNTRY_TABLE_FIELD));
        System.out.println(String.format("[6] Remove a %s", CITY_TABLE_FIELD));
        System.out.println("**********************************************");
        System.out.println(String.format("[7] Update a %s", COUNTRY_TABLE_FIELD));
        System.out.println(String.format("[8] Update a %s", CITY_TABLE_FIELD));
        System.out.println("**********************************************");
        System.out.println(String.format("[9] Show all %s and %s", CITY_TABLE, COUNTRY_TABLE));
        System.out.println("**********************************************");
        System.out.println("[b] Return back");
        System.out.println("**********************************************\n");
        System.out.print("Select: ");
    }

    public static void postgreSql() {

        Connection conn = null;

        try {

            conn = DriverManager.getConnection("jdbc:" + dbPostgres + "://" + dbHost + "/" + db, "postgres", "jupiter");

            conn.setAutoCommit(false);

            menuPostgres();

            long lines;
            String ct;
            String cnt;

            String menuItem;

            do {
                menuItem = scan.nextLine();

                switch (menuItem) {

                    case "1":
                        Statement statement = conn.createStatement();
                        try {
                            // Drop tables
                            statement.executeUpdate(String.format("DROP TABLE IF EXISTS %s,%s", COUNTRY_TABLE, CITY_TABLE));

                        } catch (SQLException psql) {
                            System.out.println("Tables don't exist");
                        }

                        // Countries table
                        statement.executeUpdate(String.format("CREATE TABLE %s ( "
                                        + "%s" + " serial CONSTRAINT PK_%s_ID PRIMARY KEY,"
                                        + "%s" + " varchar(255) NOT NULL," +
                                         "code" + " varchar(4)," +
                                        "CONSTRAINT %s_%s_UK UNIQUE(%s))", COUNTRY_TABLE, COUNTRY_TABLE_FIELD, CITY_TABLE_FIELD
                                , TABLE_NAME_FIELD, COUNTRY_TABLE, TABLE_NAME_FIELD, TABLE_NAME_FIELD));

                        // Cities table
                        statement.executeUpdate(String.format("CREATE TABLE %s ( "
                                        + "%s" + " integer ,"
                                        + "%s" + " varchar(255) NOT NULL,"
                                        + "CONSTRAINT PK_%s_FP PRIMARY KEY(%s,%s),"
                                        + "CONSTRAINT FK_%s_%s FOREIGN KEY(%s) REFERENCES %s(%s) "
                                        + "ON DELETE CASCADE ON UPDATE CASCADE)"
                                , CITY_TABLE, COUNTRY_TABLE_FIELD, TABLE_NAME_FIELD, CITY_TABLE, COUNTRY_TABLE_FIELD, TABLE_NAME_FIELD
                                , CITY_TABLE, COUNTRY_TABLE_FIELD, COUNTRY_TABLE_FIELD, COUNTRY_TABLE, COUNTRY_TABLE_FIELD));

                        conn.commit();
                        statement.close();

                        System.out.println(String.format("Tables: %s & %s successfully created", COUNTRY_TABLE, CITY_TABLE));

                        showMenu();
                        break;
                    case "2":
                        try {
                            PreparedStatement preparedStatementCountry = conn.prepareStatement(insertCountriesSql2);
                            PreparedStatement preparedStatementCity = conn.prepareStatement(insertCitiesSql);

                            WorldReader r = new WorldReader("data/world.xml");

                            ArrayList<String> citiesL = new ArrayList<>();
                            for (String p : r.listarPaises()) {
                                citiesL.clear();
                                String id = r.getPaisId(p);
                                preparedStatementCountry.setString(1, p);
                                preparedStatementCountry.setString(2, id);
                                preparedStatementCountry.addBatch();
                                System.out.println(String.format("Inserting %s " + p, COUNTRY_TABLE_FIELD));

                                for (String c : r.listarCiudadesPais(p)) {
                                    try {
                                        if (!citiesL.contains(c)) {
                                            citiesL.add(c);
                                            preparedStatementCity.setString(1, p);
                                            preparedStatementCity.setString(2, c);
                                            preparedStatementCity.addBatch();
                                            System.out.println(String.format("Inserting %s " + c, CITY_TABLE_FIELD));
                                        } else {
                                            throw new SQLException();
                                        }

                                    } catch (SQLException exC) {
                                        System.err.println(String.format("Duplicated %s " + c, CITY_TABLE_FIELD));
                                    }
                                }
                            }

                            long var = 0;

                            int[] ctrarray = preparedStatementCountry.executeBatch();

                            for (int z : ctrarray) {
                                var += z;
                            }

                            int[] ctarray = preparedStatementCity.executeBatch();

                            for (int i : ctarray) {
                                var += i;
                            }

                            try {
                                conn.commit();
                                preparedStatementCountry.close();
                                preparedStatementCity.close();
                                lines = var;
                            } catch (BatchUpdateException bue) {
                                lines = 0;
                            }

                            System.out.println("Changes: " + lines);

                        } catch (Exception e) {
                            System.err.println(e.getMessage());
                        }
                        showMenu();
                        break;
                    case "3":
                        System.out.print(String.format("Type a %s name: ", COUNTRY_TABLE_FIELD));
                        cnt = toInitCap(scan.nextLine().toLowerCase());

                        PreparedStatement preparedInsertCountry = conn.prepareStatement(insertCountriesSql);
                        preparedInsertCountry.setString(1, cnt);

                        try {
                            lines = preparedInsertCountry.executeUpdate();
                        } catch (SQLException sql) {
                            lines = 0;
                        }

                        conn.commit();

                        preparedInsertCountry.close();

                        System.out.println("Changes: " + lines);


                        showMenu();
                        break;
                    case "4":
                        System.out.print(String.format("Type a %s name: ", CITY_TABLE_FIELD));
                        ct = toInitCap(scan.nextLine().toLowerCase());

                        System.out.print(String.format("Type a %s name: ", COUNTRY_TABLE_FIELD));
                        cnt = toInitCap(scan.nextLine().toLowerCase());

                        PreparedStatement preparedInsertCity = conn.prepareStatement(insertCitiesSql);
                        preparedInsertCity.setString(1, cnt);
                        preparedInsertCity.setString(2, ct);

                        try {
                            lines = preparedInsertCity.executeUpdate();
                        } catch (SQLException sql) {
                            lines = 0;
                        }

                        conn.commit();

                        preparedInsertCity.close();

                        System.out.println("Changes: " + lines);


                        showMenu();
                        break;
                    case "5":
                        System.out.print(String.format("Type a %s name: ", COUNTRY_TABLE_FIELD));
                        cnt = toInitCap(scan.nextLine().toLowerCase());


                        PreparedStatement preparedDeleteCountry = conn.prepareStatement(deleteCountriesSql);
                        preparedDeleteCountry.setString(1, cnt);

                        try {
                            lines = preparedDeleteCountry.executeUpdate();
                        } catch (SQLException sql) {
                            lines = 0;
                        }

                        conn.commit();

                        preparedDeleteCountry.close();

                        System.out.println("Changes: " + lines);

                        showMenu();
                        break;
                    case "6":
                        System.out.print(String.format("Type a %s name: ", CITY_TABLE_FIELD));
                        ct = toInitCap(scan.nextLine().toLowerCase());

                        System.out.print(String.format("Type a %s name: ", COUNTRY_TABLE_FIELD));
                        cnt = toInitCap(scan.nextLine().toLowerCase());

                        PreparedStatement preparedDeleteCity = conn.prepareStatement(deleteCitiesSql);
                        preparedDeleteCity.setString(1, ct);
                        preparedDeleteCity.setString(2, cnt);

                        try {
                            lines = preparedDeleteCity.executeUpdate();
                        } catch (SQLException sql) {
                            lines = 0;
                        }

                        conn.commit();

                        preparedDeleteCity.close();

                        System.out.println("Changes: " + lines);

                        showMenu();
                        break;
                    case "7":
                        System.out.print(String.format("Type the old %s name: ", COUNTRY_TABLE_FIELD));
                        cnt = toInitCap(scan.nextLine().toLowerCase());


                        System.out.print(String.format("Type the new %s name: ", COUNTRY_TABLE_FIELD));
                        String cntNew = toInitCap(scan.nextLine().toLowerCase());

                        PreparedStatement preparedUpdateCountry = conn.prepareStatement(updateCountriesSql);
                        preparedUpdateCountry.setString(1, cntNew);
                        preparedUpdateCountry.setString(2, cnt);

                        try {
                            lines = preparedUpdateCountry.executeUpdate();
                        } catch (SQLException sql) {
                            lines = 0;
                        }

                        conn.commit();

                        preparedUpdateCountry.close();

                        System.out.println("Changes: " + lines);

                        showMenu();
                        break;
                    case "8":
                        System.out.print(String.format("Type the old %s name: ", CITY_TABLE_FIELD));
                        ct = toInitCap(scan.nextLine().toLowerCase());

                        System.out.print(String.format("Type its %s name: ", COUNTRY_TABLE_FIELD));
                        cnt = toInitCap(scan.nextLine().toLowerCase());


                        System.out.print(String.format("Type the new %s name: ", CITY_TABLE_FIELD));
                        String ctNew = toInitCap(scan.nextLine().toLowerCase());


                        PreparedStatement preparedUpdateCity = conn.prepareStatement(updateCitiesSql);
                        preparedUpdateCity.setString(1, ctNew);
                        preparedUpdateCity.setString(2, ct);
                        preparedUpdateCity.setString(3, cnt);

                        try {
                            lines = preparedUpdateCity.executeUpdate();
                        } catch (SQLException sql) {

                            lines = 0;
                        }

                        conn.commit();

                        preparedUpdateCity.close();

                        System.out.println("Changes: " + lines);

                        showMenu();
                        break;
                    case "9":
                        statement = conn.createStatement();

                        String selectDb = String.format("SELECT %s.%s AS %s, %s.%s AS %s " +
                                        "FROM %s,%s WHERE %s.%s=%s.%s " +
                                        "ORDER BY %s.%s, %s.%s;", COUNTRY_TABLE, TABLE_NAME_FIELD, COUNTRY_TABLE_FIELD, CITY_TABLE, TABLE_NAME_FIELD
                                , CITY_TABLE_FIELD, COUNTRY_TABLE, CITY_TABLE, COUNTRY_TABLE, COUNTRY_TABLE_FIELD, CITY_TABLE, COUNTRY_TABLE_FIELD
                                , COUNTRY_TABLE, TABLE_NAME_FIELD, CITY_TABLE, TABLE_NAME_FIELD);

                        ResultSet rsQuery = statement.executeQuery(selectDb);

                        if (rsQuery.next() == false) {
                            System.out.println("Empty db");
                        }

                        if (!select.isEmpty()) {
                            select.clear();
                        }

                        while (rsQuery.next()) {
                            String[] arr = new String[2];

                            arr[0] = rsQuery.getString(String.format("%s", COUNTRY_TABLE_FIELD));
                            arr[1] = rsQuery.getString(String.format("%s", CITY_TABLE_FIELD));

                            select.add(arr);
                        }
                        rsQuery.close();

                        int maxA = 0;
                        int maxB = 0;
                        for (String[] arr : select) {
                            if (arr[0].length() > maxA) {
                                maxA = arr[0].length();
                            }
                            if (arr[1].length() > maxB) {
                                maxB = arr[1].length();
                            }
                        }

                        if (select.isEmpty()) {
                            showMenu();
                            break;
                        }

                        select.clear();

                        String bar = "";
                        for (int i = 0; i < maxA + maxB; i++) {
                            bar += "-";
                        }

                        ResultSet rsQueryFinal = statement.executeQuery(selectDb);

                        System.out.println(bar);

                        System.out.println(String.format("%-" + maxA + "s | %-" + maxB + "s", String.format("%s", COUNTRY_TABLE_FIELD)
                                , String.format("%s", CITY_TABLE_FIELD)));

                        System.out.println(bar);

                        while (rsQueryFinal.next()) {
                            System.out.println(String.format("%-" + maxA + "s | %-" + maxB + "s", rsQueryFinal.getString(String.format("%s", COUNTRY_TABLE_FIELD))
                                    , rsQueryFinal.getString(String.format("%s", CITY_TABLE_FIELD))));
                        }
                        rsQueryFinal.close();

                        statement.close();

                        System.out.println(bar);

                        showMenu();
                        break;
                    case "m":
                        menuPostgres();
                        break;
                    case "b":
                        mainMenu();
                        break;
                    default:
                        showMenu();
                        break;
                }
            } while (!menuItem.equals("b"));

        } catch (SQLException sql) {
            System.err.println(sql.getErrorCode());
            sql.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.rollback();
                    conn.close();
                } catch (SQLException sql) {
                    System.err.println(sql.getErrorCode());
                    sql.printStackTrace();
                }
            }
        }

    }

}
